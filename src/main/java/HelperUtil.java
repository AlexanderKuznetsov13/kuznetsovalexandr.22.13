public class HelperUtil {
    public static int[] convertStrToIntArray(String str) {
        String[] strArray = str.trim().split(" ");

        int[] numArray = new int[strArray.length];// перевод стоки в целочисленное
        for (int i = 0; i < strArray.length; i++) {
            numArray[i] = Integer.parseInt(strArray[i]);
        }

        return numArray;
    }
}
