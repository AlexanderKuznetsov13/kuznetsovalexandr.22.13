package incapsulation_OOP;

//Создать класс Human используя инкапсуляцию (модификаторы и методы доступа), у которого будут поля:
//
//String name
//String lastName
//int age
//
//Будет два конструктора - один пустой, второй - полный (инициализирует все поля)

public class Human {
    private String name;
    private String lastName;
    private int age;

    public Human() {
    }

    public Human(String name, String lastName, int age) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "name = " + name + ";  last name = " + lastName + ";  age = " + age;

    }
}
