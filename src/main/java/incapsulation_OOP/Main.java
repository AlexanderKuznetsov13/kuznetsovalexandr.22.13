package incapsulation_OOP;

//Создать класс Main, в котором будет создаваться массив на случайное количество человек.
// После инициализации массива - заполнить его объектами класса Human (у каждого объекта случайное значение поля age).
// После этого - отсортировать массив по возрасту и вывести результат в консоль

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Human[] humans = getHumans();
        sortHumans(humans);

        for (Human human : humans) {
            System.out.println(human);
        }
    }

    private static void sortHumans(Human[] humans) {
        for (int i = 0; i < humans.length; i++) {
            Human humanMinAge = humans[i];
            int indexMinAge = i;

            for (int j = i + 1; j < humans.length; j++) {
                if (humans[j].getAge() < humanMinAge.getAge()) {
                    humanMinAge = humans[j];
                    indexMinAge = j;
                }
            }

            Human tmpHuman = humans[i];
            humans[indexMinAge] = tmpHuman;
            humans[i] = humanMinAge;
        }
    }

    public static Human[] getHumans() {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        int x = random.nextInt(5);
        Human[] humans = new Human[x];

        String name;
        String lastName;
        int age;

        for (int i = 0; i < humans.length; i++) {
            int numHuman = i + 1;

            System.out.print("Enter name №" + numHuman + ": ");
            name = scanner.next();
            System.out.print("Enter lastname №" + numHuman + ": ");
            lastName = scanner.next();
            age = random.nextInt(99);// максимальный возраст 99 лет

            humans[i] = new Human(name, lastName, age);
        }
        return humans;
    }
}







