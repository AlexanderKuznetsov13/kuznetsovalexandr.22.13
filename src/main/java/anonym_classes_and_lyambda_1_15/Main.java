package anonym_classes_and_lyambda_1_15;

import java.util.Arrays;
import java.util.Random;

//проверку на четность элемента

//проверку, является ли сумма цифр элемента четным числом.

//Доп. задача: проверка на четность всех цифр числа. (число состоит из цифр)

//Доп. задача: проверка на палиндромность числа (число читается одинаково и слева,
// и справа -> 11 - палиндром, 12 - нет, 3333 - палиндром, 3211 - нет, и т.д.).

public class Main {
    public static void main(String[] args) {
        int[] array = new int[10];
        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10000) + 1;
        }
        //Anonymity classes
        ByCondition conditionChet = new ByCondition() {
            //четное не четное число
            @Override
            public boolean isOk(int number) {
                return number % 2 == 0;
            }
        };

        //Anonymity classes
        ByCondition conditionSumChet = new ByCondition() {
            // сумма цифр в числе четная
            @Override
            public boolean isOk(int number) {
                int sum = 0;
                while (number != 0) {
                    //Суммирование цифр числа
                    sum += (number % 10);
                    number /= 10;
                }
                return sum % 2 == 0;
            }
        };

        //Anonymity classes
        ByCondition conditionOllChet = new ByCondition() {
            //проверка на четность всех цифр числа
            @Override
            public boolean isOk(int number) {
                int k=0;
                while (number != 0) {
                    if ((number % 10) % 2 != 0) {
                        k++;
                        break;
                    }
                    number /= 10;
                }
                return k==0;
            }

        };

        //Anonymity classes
        ByCondition conditionPalindrom = new ByCondition() {
            //проверка на палиндромность числа
            @Override
            public boolean isOk(int number) {
                int palindrome = number;
                int reverse = 0;
                while (palindrome != 0) {
                    int remainder = palindrome % 10;
                    reverse = reverse * 10 + remainder;
                    palindrome = palindrome / 10;
                }
                return number == reverse;
            }
        };
        System.out.println("Массив" + Arrays.toString(array));
        System.out.println("Четные числа в массиве:");
        Sequence.filter(array, conditionChet);
        System.out.println("Сумма цифр числа в массиве четная:");
        Sequence.filter(array, conditionSumChet);
        System.out.println("Все цифры числа четные:");
        Sequence.filter(array, conditionOllChet);
        System.out.println("Палиндромные числа массива:");
        Sequence.filter(array, conditionPalindrom);


    }
}