package anonym_classes_and_lyambda_1_15;

public interface ByCondition {
    boolean isOk(int number);
}
