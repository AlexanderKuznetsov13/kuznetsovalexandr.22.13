package anonym_classes_and_lyambda_1_15;

public class Sequence {

    public static int[] filter(int[] array, ByCondition condition) {
        for (int i = 0; i < array.length; i++) {
            //Если условие верно, то распечатываем элемент
            if (condition.isOk(array[i])) {
                System.out.println(array[i]);
            }
        }
        return array;
    }
}