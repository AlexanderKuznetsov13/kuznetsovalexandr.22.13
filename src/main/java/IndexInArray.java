
import java.util.Scanner;

//Реализовать функцию, принимающую на вход массив и целое число.
// Данная функция должна вернуть индекс этого числа в массиве.
// Если число в массиве отсутствует - вернуть -1.

public class IndexInArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите массив через пробел");
        String input = scanner.nextLine();

        int[] numArray = HelperUtil.convertStrToIntArray(input);

        System.out.println("Введите число, которое нужно найти в массиве");
        int number = scanner.nextInt();

        int index = findArrayIndexByElement(numArray, number);
        System.out.println("Индекс числа в массиве: " + index);
    }

    public static int findArrayIndexByElement(int[] array, int number) {
        int k = -1;
        for (int i = 0; i < array.length; i++)
            if (array[i] == number) {
                k = 1;
                return i;
            }
        return k;
    }
}
