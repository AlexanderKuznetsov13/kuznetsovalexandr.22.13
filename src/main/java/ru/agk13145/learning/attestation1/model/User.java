package ru.agk13145.learning.attestation1.model;

import java.util.Objects;

public class User {
    private int id;
    private String name;
    private String surname;
    private int age;
    private boolean isWorking;

    public User(int id, String name, String surname, int age, boolean isWorking) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.isWorking = isWorking;
    }

    public User(String name, String surname, int age, boolean isWorking) {
        this(0, name, surname, age, isWorking);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isWorkng() {
        return isWorking;
    }

    public void setWorking(boolean isWorking) {
        this.isWorking = isWorking;
    }

    @Override
    public String toString() {
        String value = String.format("%d|%s|%s|%d|%b", id, name, surname, age, isWorking);
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id && age == user.age && isWorking == user.isWorking && name.equals(user.name) && surname.equals(user.surname);
    }

    public boolean equalsIgnoreIdAndWork(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return age == user.age && name.equals(user.name) && surname.equals(user.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, age, isWorking);
    }
}