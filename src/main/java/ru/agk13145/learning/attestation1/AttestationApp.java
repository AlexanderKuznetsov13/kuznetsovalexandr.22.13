package ru.agk13145.learning.attestation1;

import ru.agk13145.learning.attestation1.model.User;
import ru.agk13145.learning.attestation1.service.IUserService;
import ru.agk13145.learning.attestation1.service.impl.UsersRepositoryFileImpl;

public class AttestationApp {
    public static void main(String[] args) {
        IUserService userService = new UsersRepositoryFileImpl();
        User user1 = new User("Sem", "Smith", 23, true);
        User user2 = new User("Alex", "Opal", 36, true);
        User user3 = new User("Ilia", "Kuznetsov", 14, false);
        userService.create(user1);
        userService.create(user2);
        userService.create(user3);

        User user4 = new User(4, "Ilia", "Kuznetsov", 14, true);
        userService.update(user4);

        user4.setId(3);
        userService.update(user4);

        userService.delete(2);

        User seachedUserById = userService.findById(1);

    }
}
