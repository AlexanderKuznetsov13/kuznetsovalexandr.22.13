package ru.agk13145.learning.attestation1.service;

import ru.agk13145.learning.attestation1.model.User;

/*
Реализовать в классе UsersRepositoryFileImpl методы:
        User findById(int id); - поиск юзера по ID
        void create(User user); - создание нового юзера (запись в файл)
        void update(User user); - обновление информации по юзеру
        void delete(int id); - удаление юзера по ID


 */
public interface IUserService {

    //поиск юзера по ID
    User findById(int id);

    //создание нового юзера (запись в файл)
    void create(User user);

    //обновление информации по юзеру
    void update(User user);

    //удаление юзера по ID
    void delete(int id);

}
