package ru.agk13145.learning.attestation1.service.impl;

import ru.agk13145.learning.attestation1.model.User;
import ru.agk13145.learning.attestation1.service.IUserService;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class UsersRepositoryFileImpl implements IUserService {

    private static int counter = 0;


    @Override
    public User findById(int id) {
        List<User> users = readUsersFromFile();
        boolean isNotExisted = true;
        for (User tmpUser : users) {
            if (tmpUser.getId() == id) {
                System.out.println(" Найден пользователь с идентификатором " + id );
                return tmpUser;
            }
        }
        System.out.println(" Пользователь с идентификатором " + id + "  не найден");
        return null;
    }

    @Override
    public void create(User user) {
        List<User> users = readUsersFromFile();
        boolean isNotExisted = true;
        for (User tmpUser : users) {
            if (tmpUser.equalsIgnoreIdAndWork(user)) {
                isNotExisted = false;
                break;
            }
        }
        if (isNotExisted) {
            if (users.isEmpty()) {
                counter++;
            } else {
                User tmpUser = users.get(users.size() - 1);
                counter = tmpUser.getId();
                counter++;
            }
            user.setId(counter);
            users.add(user);
            saveUserToFile(users);
        }
    }

    private void saveUserToFile(List<User> users) {
        System.out.println("Запись всех пользователей в файл, количество пользователей: " + users.size());
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("Users.txt", false))) {
            for (User user : users) {
                writer.write(user.toString());
                writer.write("\n");
            }
            writer.flush();
        } catch (IOException e) {
            System.out.println("Произошла ошибка " + e.getMessage());
            throw new RuntimeException();
        }
    }

    private List<User> readUsersFromFile() {
        System.out.println("Считывание всех пользователей из файла");
        List<User> users = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader("Users.txt"))) {
            String line = null;

            while ((line = reader.readLine()) != null) {
                //System.out.println(line);
                if ("".equals(line))
                    continue;

                String[] userInfo = line.split("\\|");
                User user = new User(Integer.parseInt(userInfo[0]), userInfo[1], userInfo[2], Integer.parseInt(userInfo[3]), Boolean.parseBoolean(userInfo[4]));
                users.add(user);
            }
        } catch (IOException e) {
            throw new RuntimeException();
        }
        System.out.println("Количество считываемых пользователей: " + users.size());
        return users;
    }

    @Override
    public void update(User user) {
        List<User> users = readUsersFromFile();
        int idxSearchedUser = getIdxFromListByUserId(users, user.getId());

        if (idxSearchedUser == -1) {
            System.out.println("  Пользователь с идентификатором " + user.getId() + " не найден");
        } else {
            User userFromList = users.get(idxSearchedUser);
            userFromList.setName(user.getName());
            userFromList.setSurname(user.getSurname());
            userFromList.setAge(user.getAge());
            userFromList.setWorking(user.isWorkng());
            saveUserToFile(users);
            System.out.println("  Пользователь с идентификатором " + user.getId() + " обновлен");
        }

    }

    @Override
    public void delete(int id) {
        List<User> users = readUsersFromFile();
        int idxSearchedUser = getIdxFromListByUserId(users, id);

        if (idxSearchedUser == -1) {
            System.out.println("  Пользователь с идентификатором " + id + " не найден");
        } else {
            User user1 = users.get(idxSearchedUser);
            users.remove(idxSearchedUser);
            saveUserToFile(users);
            System.out.println("  Пользователь с идентификатором " + id + " удален");
        }
    }
    private int  getIdxFromListByUserId (List<User> users, int id ){
        for (int i = 0 ; i < users.size();i++){
            if (users.get(i).getId() == id) {
                return i;
            }
        }
        return -1;
    }
}
