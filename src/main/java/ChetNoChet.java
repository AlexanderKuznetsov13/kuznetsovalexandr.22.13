import java.util.Scanner;

/*Есть, неопределенная в количестве (т.е. чисел может быть и 5, и 100, и 500 и т.д.), последовательность чисел:
        а0, а1, а2 ... аN
        N -> бесконечность, aN -> -1 (последнее число в последовательности всегда -1)
Нужно написать код алгоритма, который посчитает количество всех четных чисел и нечетных в последовательности.
        Под конец алгоритма вывести количество четных и нечетных чисел.
        Подсказка: для выполнения данной задачи нужно воспользоваться оператором %
*/
public class ChetNoChet {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        int[] numArray = HelperUtil.convertStrToIntArray(input);

        int[] result = chetNoChet(numArray);

        System.out.println(("Четных числе в массиве = " + result[0] + "  " + "Нечетных числе в массиве = " + result[1]));

    }



    public static int[] chetNoChet(int[] array) {
        int chet = 0;
        int noChet = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0)
                chet++;
            else
                noChet++;
        }

        return new int[]{chet, noChet};

    }
}
