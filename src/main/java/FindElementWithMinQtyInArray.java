import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * На вход подается последовательность чисел, стремящаяся к бесконечности (последовательность может быть и из 100 чисел, может и их 1000000 чисел быть), оканчивающаяся на -101.
 * Необходимо вывести число, которое присутствует в последовательности минимальное количество раз.
 * Условия задачи:
 *
 *
 * Все числа в диапазоне от -100 до 100.
 *
 *
 * Числа встречаются не более 2 147 483 647-раз каждое.
 *
 *
 * Сложность алгоритма - O(n)
 */

public class FindElementWithMinQtyInArray {

    public static void main(String[] args) {
        int[] array = getArray();
        System.out.println("Минимальное количество повторений в массиве у числа: " + getMinOfSeries(array));
    }


    /**
     * метод запрашивает у пользователя вводить числа от -100 до 100, пока не будет введено -101
     */
    static int[] getArray() {
        List<Integer> list = new ArrayList<>();

        System.out.println("Вводите числа от -100 до 100. Ввод прекратится, если Вы введёте -101");

        Scanner scanner = new Scanner(System.in);
        int inputValue = scanner.nextInt();

        while (inputValue != -101) {
            if (inputValue > -101 && inputValue < 101)
                list.add(inputValue);
            else
                System.out.println("Некорректное число. Вводите числа от -100 до 100");
            inputValue = scanner.nextInt();
        }

        //конвертирование в массив примитивов
        int[] array = new int[list.size()];

        for (int i = 0; i < array.length; i++) {
            array[i] = list.get(i);
        }

        return array;
    }

    /**
     * метод возвращает число, которое меньшее количество раз повторяется;
     */
    static int getMinOfSeries(int[] arrayInput) {
        if (arrayInput.length == 0)
            throw new RuntimeException("Массив пустой");

        int[] arrayOfNumOfDigits = getArrayOfNumOfDigits(arrayInput);

        //находим минимальную последовательность
        int min = Integer.MAX_VALUE;
        int value = -100;
        for (int i = 0; i < arrayOfNumOfDigits.length; i++) {
            if (arrayOfNumOfDigits[i] != 0) {
                if (arrayOfNumOfDigits[i] < min) {
                    min = arrayOfNumOfDigits[i];
                    value = i - 100;
                }
            }
        }
        return value;
    }


    /**
     * формируем массив с количеством повторений
     */
    static int[] getArrayOfNumOfDigits (int [] arrayInput){
        int[] arrayOfNumOfDigits = new int[201];
        for (int i = 0; i < arrayInput.length; i++) {
            if (arrayInput[i] > -101 && arrayInput[i] < 101) {
                arrayOfNumOfDigits[arrayInput[i] + 100]++;
            }
        }
        return arrayOfNumOfDigits;
    }


}
