import java.util.Scanner;

/*Есть, неопределенная в количестве (т.е. чисел может быть и 5, и 100, и 500 и т.д.), последовательность чисел:
        а0, а1, а2 ... аN
        N -> бесконечность, aN -> -1 (последнее число в последовательности всегда -1)
        Нужно написать код алгоритма нахождения минимального числа среди всей последовательности.
        Под конец алгоритма вывести минимальное число.
*/
public class MinNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        String[] strArray = s.split("");

        int[] numArray = new int[strArray.length];// перевод стоки в целочисленное
        for (int i = 0; i < strArray.length; i++) {
            numArray[i] = Integer.parseInt(strArray[i]);
        }

      System.out.println(Minimum(numArray));

    }
    public static int Minimum(int[] array){
        int min=array[0];
        for (int i=0;i<=array.length;i++)
        if (array[i]!=-1){
            if (min > array[i]){
                min=array[i];
            }
        }
    return min;
    }
}
