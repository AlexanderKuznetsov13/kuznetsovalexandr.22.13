package inheritance;

public class Worker {
    private String name;
    private String lastname;
    private String profession;


    public Worker() {

    }

    public Worker(String name, String lastname, String profession) {
        this.name = name;
        this.lastname = lastname;
        this.profession = profession;
    }

    public void goToWork() {
        System.out.println(getName() + " " + getLastname() + " " + getProfession() + " \n" +
                "Работает как может и чему обучен.");
    }

    public void goToVacation(int days) {
        System.out.println(getProfession() + " " + getName() + " " + getLastname() + "\n" +
                "имеет право на отпуск");
    }


    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public String getProfession() {
        return profession;
    }

}
