package inheritance;

//Нужно создать класс Worker, с полями:
//
//String name; // имя
//String lastName; // фамилия
//String profession; // профессия
//
//Так же у класса Worker, есть два метода:
//
//public void goToWork(); //Метод в котором описана работа.
// Метод, как минимум, должен выводить кто работает, какая у него профессия и как он работает.
// Каждый потомок должен переопределить этот метод (каждая профессия работает по своему)

//public void goToVacation(int days); // Метод в котором описан отпуск.
// На вход принимает количество дней отпуска. Метод, как минимум, должен выводить кто уходит в отпуск,
//
//  Каждый потомок должен переопределить этот метод (каждая профессия отдыхает по своему)
//
//Нужно создать минимум четыре класса потомка (профессию выбирайте сами:)),
// например - Программист, Тестировщик, СисАдмин, ДевОпс и.т.д.

//В классе Main (в котором создаются объекты всех классов профессий)
// через полиморфизм вывести отпуск и работу каждого объекта по аналогии с "машинами" на уроке.


public class Main {
    public static void main(String[] args) {
        Worker builder = new Builder("Stiv", "Karllson", "Builder", 28);
        Worker doctor = new Doctor("Martin", "Jonson", "doctor", 28);
        Worker teacher = new Teacher("Katrin", "Hill", "teacher", 56);
        Worker tractorDriver = new TractorDriver("Jon", "Willson", "tractor driver", 34);

        builder.goToWork();
        builder.goToVacation(28);
        doctor.goToWork();
        doctor.goToVacation(28);
        teacher.goToWork();
        teacher.goToVacation(56);
        tractorDriver.goToWork();
        tractorDriver.goToVacation(34);


    }

}
