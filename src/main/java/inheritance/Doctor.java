package inheritance;

public class Doctor extends Worker {

    private int vacationDays;


    public Doctor(String name, String lastname, String profession, int vacationDays) {
        super(name, lastname, profession);
        this.vacationDays = vacationDays;
    }

    @Override
    public void goToWork() {
        System.out.println(getName() + " " + getLastname() + " " + getProfession()  +
                "  профессиональную медицинскую помощь людям");
    }

    @Override
    public void goToVacation(int vacationDays) {
        System.out.println(getProfession() + " " + getName() + " " + getLastname()  +
                "имеет право на отпуск в размере " + vacationDays + " календарных дней в году\n");
    }
}
