package inheritance;

public class Teacher extends Worker {

    private int vacationDays;

    public Teacher(String name, String lastname, String profession, int vacationDays) {
        super(name, lastname, profession);
        this.vacationDays = vacationDays;
    }

    @Override
    public void goToWork() {
        System.out.println(getName() + " " + getLastname() + " " + getProfession()  +
                " Знакомит с новыми знаниями детей и не только детей");
    }

    @Override
    public void goToVacation(int vacationDays) {
        System.out.println(getProfession() + " " + getName() + " " + getLastname()  +
                "имеет право на отпуск в размере " + vacationDays + " календарных дней в году\n");
    }
}
