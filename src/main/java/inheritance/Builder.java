package inheritance;

public class Builder extends Worker {

    private int vacationDays;

    public Builder(String name, String lastname, String profession, int vacationDays) {
        super(name, lastname, profession);
        this.vacationDays = vacationDays;
    }


    @Override
    public void goToWork() {
        System.out.println(getName() + " " + getLastname() + " " + getProfession()  +
                " Работает на строительстве зданий и объектов промышленного назначения");
    }

    @Override
    public void goToVacation(int vacationDays) {
        System.out.println(getProfession() + " " + getName() + " " + getLastname()  +
                "имеет право на отпуск в размере " + vacationDays + " календарных дней в году\n");
    }
}
