import java.util.Arrays;
import java.util.Scanner;

//Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые, например:
//
//Было:
//34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20
//Стало после применения процедуры:
//34, 14, 15, 18, 1, 20, 0, 0, 0, 0, 0


public class ChangeZeroInArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        int[] numArray = HelperUtil.convertStrToIntArray(input);
        changeZeroInArray(numArray);

        for (int a : numArray) {
            System.out.print(a + " ");
        }

    }




    public static void changeZeroInArray(int[] array) {
        int idxNumbers = 0;
        int cntZero = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != 0) {
                array[idxNumbers] = array[i]; //save number
                idxNumbers++; //increment index
                cntZero = 0; //reset zero counter
            } else {
                cntZero++; //count zero
            }
        }

        while (idxNumbers < array.length) {
            array[idxNumbers] = 0;
            idxNumbers++;
        }


    }

}
