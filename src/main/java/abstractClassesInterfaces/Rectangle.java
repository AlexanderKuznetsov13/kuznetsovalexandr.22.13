package abstractClassesInterfaces;

public class Rectangle extends Figure {
    private double a;// сторона 1
    private double b;// сторона 2

    public Rectangle(Integer x, Integer y, double a, double b) {
        super(x, y, "Rectangle");
        this.a = a;
        this.b = b;
    }

    @Override
    public double getPerimeter() {
        //P = 2a+2b
        //a - сторона 1
        //b - сторона 2
        return 2 * (this.a + this.b);
    }

    @Override
    public String getName() {
        return super.getName();
    }
}
