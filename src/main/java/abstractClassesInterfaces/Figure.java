package abstractClassesInterfaces;


public abstract class Figure {
    private Integer x;
    private Integer y;
    private String name;


    public Figure(Integer x, Integer y, String name) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public abstract double getPerimeter();//

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
