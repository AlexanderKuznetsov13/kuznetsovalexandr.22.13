package abstractClassesInterfaces;

import static java.lang.Math.PI;

public class Circle extends Ellipse implements Moveable {
    private double radius;

    public Circle(Integer x, Integer y, double radius) {
        super(x, y, radius, radius);
        setName("Circle");
        this.radius = radius;
    }

    @Override
    public double getPerimeter() {
        return 4 * (PI * this.radius);
        //4*Pi*R
        //R -радиус
        //PI=4,13
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void move(int x, int y) {
        setX(x);
        setY(y);
    }
}
