package abstractClassesInterfaces;

//Сделать абстрактный класс Figure, у данного класса есть два поля - x и y координаты.
//Классы Ellipse и Rectangle должны быть потомками класса Figure.

//Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
//В классе Figure предусмотреть абстрактный метод getPerimeter().

//Так же, нужно определить интерфейс Moveable c единственным методом .move(int x, int y),
// который позволит перемещать фигуру на заданные координаты.
//Данный интерфейс должны реализовать только классы Circle и Square.

//В Main создать массив всех фигур и "перемещаемых" фигур.

// У всех вывести в консоль периметр, а у "перемещаемых" фигур изменить случайным образом координаты.


import java.util.Random;

public class Main {
    public static void main(String[] args) {

        Ellipse ellipse = new Ellipse(6, 7, 2.0, 1.0);
        Rectangle rectangle = new Rectangle(9, 4, 6, 8);
        Circle circle = new Circle(4, 6, 4.0);
        Square square = new Square(2, 9, 4);

        Figure[] figures = new Figure[]{ellipse, rectangle, circle, square};

        Random random = new Random();
        circle.move(random.nextInt(10), random.nextInt(10));
        square.move(random.nextInt(10), random.nextInt(10));


        for (Figure figure : figures) {
            System.out.println(figure.getName() + ". координаты по x = " + figure.getX() + ", координаты по у = " +
                    figure.getY() + ". Периметр равен  " + figure.getPerimeter());
        }
    }

}
