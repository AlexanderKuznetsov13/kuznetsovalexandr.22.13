package abstractClassesInterfaces;

public interface Moveable {

    void move(int setX, int setY);
}
