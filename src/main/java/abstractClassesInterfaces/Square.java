package abstractClassesInterfaces;

public class Square extends Rectangle implements Moveable {
    private double side;

    public Square(Integer x, Integer y, double side) {
        super(x, y, side, side);
        this.setName("Square");
        this.side = side;
    }

    @Override
    public double getPerimeter() {
        super.getPerimeter();
        //4*a
        //a - сторона квадрата
        return 4*this.side;
    }

    @Override
    public void move(int x, int y) {
        setX(x);
        setY(y);
    }

    @Override
    public String getName() {
        return super.getName();
    }
}
