package abstractClassesInterfaces;

import static java.lang.Math.PI;

public class Ellipse extends Figure {
    private double a;//большая полуось
    private double b;//малая полуось

    public Ellipse(Integer x, Integer y, double a, double b) {
        super(x, y, "Ellipse");
        this.a = a;
        this.b = b;
    }

    @Override
    public double getPerimeter() {
        //P = 4(PI*a*b+(a-b)^2)/(a+b)
        // a - большая полуось
        //b-малая полуось
        //pi =3.14
        return 4 * ((PI * a * b) + (a - b) * (a - b)) / (a + b);// допилить обязательно   !!!
    }

    @Override
    public String getName() {
        return  super.getName();
    }
}

