import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HelperUtilTest {
    @Test
    @DisplayName("Конвертация строки в массив")
    void testConvertionStringToIntArray() {
        String input = "1 2 3 4 5";
        int[] expectedArray = new int[]{1, 2, 3, 4, 5};
        int[] actualArray = HelperUtil.convertStrToIntArray(input);
        Assertions.assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    @DisplayName("Конвертация строки в массив c пробелом в конце или в начале")
    void testConvertionStringToIntArrayWithSpaces() {
        String input = " 1 2 3 4 5 ";
        int[] expectedArray = new int[]{1, 2, 3, 4, 5};
        int[] actualArray = HelperUtil.convertStrToIntArray(input);
        Assertions.assertArrayEquals(expectedArray, actualArray);
    }


}