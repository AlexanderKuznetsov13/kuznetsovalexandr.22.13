import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FindElementWithMinQtyInArrayTest {

    @Test
    @DisplayName("Счетчик подсчета повторений")
    void test1() {
        int[] inputArray = new int[]{1, 3, 4, 5, 3, 1, 3, 3};
        int[] expectedArray = new int[201];
        expectedArray[101] = 2;
        expectedArray[103] = 4;
        expectedArray[104] = 1;
        expectedArray[105] = 1;

        Assertions.assertArrayEquals(expectedArray, FindElementWithMinQtyInArray.getArrayOfNumOfDigits(inputArray));
    }

    @Test
    @DisplayName("Нахождение числа с минимальным количеством повторений")
    void test2() {
        int[] inputArray = new int[]{1, 3, 4, 5, 3, 1, 2, 3, 2};
        Assertions.assertEquals(4, FindElementWithMinQtyInArray.getMinOfSeries(inputArray));

    }

    @Test
    @DisplayName("Выбросить ошибку, если массив пустой")
    void test3() {
        int[] inputArray = new int[]{};
        Assertions.assertThrows(RuntimeException.class,
                () -> FindElementWithMinQtyInArray.getMinOfSeries(inputArray),
                "Массив пустой");
    }
}