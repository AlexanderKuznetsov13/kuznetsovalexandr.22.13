import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChangeZeroInArrayTest {

    @Test
    @DisplayName("Тест на перемещение нулей в конец массива")
    void test1() {
        int[] inputArray = new int[]{3, 0, 7, 0, 0, 8, 5};
        int[] expectedArray = new int[]{3, 7, 8, 5, 0, 0, 0};
        ChangeZeroInArray.changeZeroInArray(inputArray);
        Assertions.assertArrayEquals(expectedArray, inputArray);

    }

    @Test
    @DisplayName("Тест на массив нулей")
    void test2() {
        int[] inputArray = new int[]{0, 0, 0, 0, 0};
        int[] expectedArray = new int[]{0, 0, 0, 0, 0};
        ChangeZeroInArray.changeZeroInArray(inputArray);
        Assertions.assertArrayEquals(expectedArray, inputArray);

    }

}