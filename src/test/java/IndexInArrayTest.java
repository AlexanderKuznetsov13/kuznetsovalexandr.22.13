import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IndexInArrayTest {

    @Test
    @DisplayName("Поиск несуществующего числа в массиве")
    void test1() {
        int[] inputArray = new int[]{1, 2, 6, 7, 9};
        int searchedElement = 3;
        int actualResult = IndexInArray.findArrayIndexByElement(inputArray, searchedElement);
        int expected = -1;
        Assertions.assertEquals(expected, actualResult);
    }

    @Test
    @DisplayName("Поиск индекса существующего числа в массиве")
    void test2() {
        int[] inputArray = new int[]{1, 2, 3, 6, 8, 9};
        int searchedElement = 6;
        int actualResult = IndexInArray.findArrayIndexByElement(inputArray, searchedElement);
        int expected = 3;
        Assertions.assertEquals(expected, actualResult);
    }

}