import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChetNoChetTest {




    @Test
    @DisplayName("Получение четных и нечетных чисел из массива")
    void testGetChetNochetInArray() {
        int[] inputArray = new int[]{1, 2, 3, 4, 5, 6, 7};
        int[] expectedArray = new int[]{3, 4};
        int[] actualResult = ChetNoChet.chetNoChet(inputArray);
        Assertions.assertArrayEquals(expectedArray, actualResult);//сравнение всего массива

        Assertions.assertEquals(expectedArray[0], actualResult[0]);//сравнение по элементам
        Assertions.assertEquals(expectedArray[1], actualResult[1]);//сравнение по элементам
    }


}